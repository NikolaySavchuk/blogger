<?php

namespace Rest\ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Blogger\BlogBundle\Entity\Blog;
use Blogger\BlogBundle\Entity\Comment;
use Blogger\BlogBundle\Form\CommentType;
use Blogger\BlogBundle\Form\BlogType;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;


class ApiController extends FOSRestController
{
 
    
    /**
     * @return array
     * @REST\QueryParam(name="limit", requirements="\d+", default="10", description="limit number")
     */
    public function getBlogsAction(Request $request = null, ParamFetcher $paramFetcher)
    {
        $limit = $paramFetcher->get('limit');
        $blogs = $this->getDoctrine()->getRepository('BloggerBlogBundle:Blog')->getLatestBlogs($limit);
        return $this->view($blogs);
    }
    
    /**
     * @return array
     * @Rest\View()
     */
    public function getBlogAction(Blog $blog)
    {
        return [
            'blog' => $blog
        ];
    }
    
    /**
     * @return array
     * @Rest\View()
     */
    public function postBlogAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $blog = new Blog();
        $form = $this->createForm(new BlogType(), $blog);
        $form->bind($data);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blog);
            $em->flush();
            return $this->getBlogsAction();
        } else {
            return new View($form);
        }
    }
    
    /**
     * @return array
     * @Rest\View()
     * 
     */
    public function putBlogAction($id, Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $blog = $this->getDoctrine()->getRepository('BloggerBlogBundle:Blog')->find($id);
        
        if ($blog === null) {
            $blog = new Blog();
        }
        
        $form    = $this->createForm(new BlogType(), $blog);
        $form->bind($data);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($blog);
            $em->flush();
            return $this->getBlogsAction();
        } else {
            return new View($form);
        }
    }
    
    /**
     * @return array
     * @Rest\View(statusCode=204)
     * 
     */
    public function deleteBlogAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository('BloggerBlogBundle:Blog')->find($id);
        
        if ($blog === null) {
            return 'Blog not Found';
        }
        
        $em->remove($blog);
        $em->flush();
        return $this->getBlogsAction();
    }
    
    /**
     * @return array
     * @Rest\View()
     * 
     */
    public function getCommentsAction($blog_id)
    {
        $comments = $this->getDoctrine()->getRepository('BloggerBlogBundle:Comment')->getCommentsForBlog($blog_id);
        
        return [
            'comments' => $comments
        ];
    }
    
    public function postCommentAction($blog_id, Request $request)
    {
        
        $blog = $this->getDoctrine()->getRepository('BloggerBlogBundle:Blog')->find($blog_id);
        
        if ($blog === null) {
            return 'Blog not Found';
        }
        
        $data = json_decode($request->getContent(), true);
        $comment  = new Comment();
        $comment->setBlog($blog);
        $form    = $this->createForm(new CommentType(), $comment);
        $form->bind($data);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->getCommentsAction($blog_id);
        } else {
            return new View($form);
        }
    }
    
    /**
     * @return array
     * @Rest\View()
     * 
     */
    public function putCommentAction($id, Request $request)
    {
        $comment = $this->getDoctrine()->getRepository('BloggerBlogBundle:Comment')->find($id);
        
        if ($comment === null) {
            $comment = new Comment();
        }
        
        $data = json_decode($request->getContent(), true);
        $form    = $this->createForm(new CommentType(), $comment);
        $form->bind($data);
        $blog_id = $comment->getBlog()->getId();
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->getCommentsAction($blog_id);
        } else {
            return new View($form);
        }
    }
    
    /**
     * @return array
     * @Rest\View(statusCode=204)
     * 
     */
    public function deleteCommentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $comment = $em->getRepository('BloggerBlogBundle:Comment')->find($id);
        
        if ($comment === null) {
            return 'Comment not Found';
        }
        
        $blog_id = $comment->getBlog()->getId();
        
        $em->remove($comment);
        $em->flush();
        return $this->getCommentsAction($blog_id);
    }
}