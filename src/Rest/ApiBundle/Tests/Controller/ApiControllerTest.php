<?php
// src/Blogger/BlogBundle/Tests/Controller/PageControllerTest.php

namespace Rest\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testGetBlogs()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/blogs', array(), array(), array('HTTP_ACCEPT' => 'application/json'));

        // Check there are some blog entries on the page
        $this->assertTrue($crawler->filter('article.blog')->count() > 0);
    }
}