<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Form\EnquiryType;
use Blogger\BlogBundle\Entity\Blog;
use Blogger\BlogBundle\Form\BlogType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PageController extends Controller
{
    /**
     * @Route("/tag/{tag}", name="blogger_page_tag")
     * @Route("/", name="blogger_page_index")
     * 
     * @Method("GET")
     * @Template("BloggerBlogBundle:Page:index.html.twig")
     */
    public function indexAction($tag = null)
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($tag === null) {
            $blogs = $em->getRepository('BloggerBlogBundle:Blog')->getLatestBlogs();
        } else {
            $blogs = $em->getRepository('BloggerBlogBundle:Blog')->getBlogsByTag($tag);
        }
        
        return [
            'blogs' => $blogs
        ];
    }
    
    /**
     * @Route("/about", name="blogger_page_about")
     * @Method("GET")
     * @Template("BloggerBlogBundle:Page:about.html.twig")
     */
    public function aboutAction()
    {
        return [];
    }
    
    /**
     * @Route("/contact", name="blogger_page_contact")
     * @Method("GET|POST")
     * @Template("BloggerBlogBundle:Page:contact.html.twig")
     */
    public function contactAction()
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);
        
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            
            if ($form->isValid()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact enquiry from symblog')
                    ->setFrom('enquiries@symblog.co.uk')
                    ->setTo($this->container->getParameter('blogger_blog.emails.contact_email'))
                    ->setBody($this->renderView('BloggerBlogBundle:Page:contactEmail.txt.twig', ['enquiry' => $enquiry]));
                $this->get('mailer')->send($message);
                
                $this->get('session')->getFlashBag()->add(
                    'blogger-notice', 'Your contact enquiry was successfully sent. Thank you!'
                );

                // Редирект - это важно для предотвращения повторного ввода данных в форму,
                // если пользователь обновил страницу.
                return $this->redirect($this->generateUrl('blogger_page_contact'));
            }
        }
        
        return [
            'form' => $form->createView()
        ];
    }
    
    /**
     * @Template("BloggerBlogBundle:Page:sidebar.html.twig")
     */
    public function sideBarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository('BloggerBlogBundle:Blog')->getTags();
        $tagWeights = $em->getRepository('BloggerBlogBundle:Blog')->getTagWeights($tags);
        $commentLimit = $this->container->getParameter('blogger_blog.comments.latest_comment_limit');
        $latestComments = $em->getRepository('BloggerBlogBundle:Comment')->getLatestComments($commentLimit);
        
        return [
            'latestComments' => $latestComments,
            'tags' => $tagWeights
        ];
    }
    
    /**
     * @Route("/add", name="blogger_page_post")
     * 
     * @Method("GET|POST")
     * @Template("BloggerBlogBundle:Blog:addBlog.html.twig")
     */
    public function addBlogAction()
    {
        
        $blog = new Blog();
        $form = $this->createForm(new BlogType(), $blog);
        
        $request = $this->getRequest();
        
        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($blog);
                $em->flush();
                
                $this->get('session')->getFlashBag()->add(
                    'blogger-notice', 'Ваша статья опубликована.'
                );

                // Редирект - это важно для предотвращения повторного ввода данных в форму,
                // если пользователь обновил страницу.
                return $this->redirect($this->generateUrl('blogger_page_post'));
            }
        }
        
        return [
            'form' => $form->createView()
        ];
    }
}