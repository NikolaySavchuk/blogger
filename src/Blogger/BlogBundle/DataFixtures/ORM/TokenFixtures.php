<?php
// src/Blogger/BlogBundle/DataFixtures/ORM/BlogFixtures.php

namespace Blogger\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Blogger\BlogBundle\Entity\Token;
use Blogger\BlogBundle\Entity\User;

class TokenFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $token1 = new Token();
        $token1->setToken(uniqid());
        $token1->setUser($this->getReference('user-1'));
        $manager->persist($token1);

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}