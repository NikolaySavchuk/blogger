<?php
// src/Blogger/BlogBundle/DataFixtures/ORM/BlogFixtures.php

namespace Blogger\BlogBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Blogger\BlogBundle\Entity\User;
use Blogger\BlogBundle\Entity\Role;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // создание роли ROLE_ADMIN
        $role = new Role();
        $role->setName('ROLE_USER');
        
        $manager->persist($role);
        
        $user1 = new User();
        $user1->setUsername('admin');
        $user1->setEmail('joe.doe@mail.com');
        $user1->setPassword('1223334');
        $user1->getUserRoles()->add($role);
        
        $manager->persist($user1);
        
        // шифрует и устанавливает пароль для пользователя,
        // эти настройки совпадают с конфигурационными файлами
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword('admin', uniqid());
        $user1->setPassword($password);
 
        $manager->persist($user1);
        

        $manager->flush();
        
        $this->addReference('user-1', $user1);
    }

    public function getOrder()
    {
        return 3;
    }
}